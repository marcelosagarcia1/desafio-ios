//
//  ViewController.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/4/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import CCBottomRefreshControl

class RepositoryViewController: UIViewController {
    var array:NSMutableArray? = NSMutableArray()
    var indexOfPageToRequest = 1
    let refreshControl = UIRefreshControl()
    
    @IBOutlet var tableview: UITableView!
    //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBottomPullRefresh()
        loadData(page: indexOfPageToRequest)
        AppHelper.shared.hudManager.showHud()
    }
     //MARK: Configuration Methods
    
    func loadData(page:Int){
        let vc = RepositoriesClient()
        vc.delegate = self
        vc.get(page: page)
    }
    
    func configureBottomPullRefresh(){
        refreshControl.triggerVerticalOffset = 100
        refreshControl.addTarget(self, action: #selector(refresh), for:.valueChanged)
        tableview.bottomRefreshControl = refreshControl
        
    }
    
    func refresh(){
        refreshControl.endRefreshing()
        indexOfPageToRequest += 1
        loadData(page: indexOfPageToRequest)
    }
    
     //MARK: Segue Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue" {
            let vc: PullRequestViewController = segue.destination as! PullRequestViewController
            let indexPath = self.tableview.indexPathForSelectedRow
            let obj = array?[(indexPath?.row)!] as! Repository
            vc.name = obj.owner?.login
            vc.repo = obj.name
        }
    }
}

    //MARK: Client Delegate Methods

extension RepositoryViewController: RepositoriesClientDelegate {
    func success(response: [Repository]) {
        array?.addObjects(from: response)
        tableview.reloadData()
        AppHelper.shared.hudManager.hideHud()
    }
     
    func error(error: NSString) {
        AppHelper.shared.alertManager.showAlertError(error: error)
        AppHelper.shared.hudManager.hideHud()
    }
}
     //MARK: TableView Methods

extension RepositoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as! RepositoryTableViewCell
        if let array = array {
            cell.populate(object: array[indexPath.row] as! Repository)
        }
        
        return cell
    }
    
    func tableView(_ taxbleView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segue", sender: self)
    }
}
