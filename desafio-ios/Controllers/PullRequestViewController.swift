//
//  PullRequestViewController.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 05/01/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController {
    var name: String? = nil
    var repo: String? = nil
    var array:NSMutableArray? = NSMutableArray()
    var indexOfPageToRequest = 1
    
    @IBOutlet weak var tableView: UITableView!
     //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData(name: name!, repo: repo!)
        tableView.tableFooterView = UIView()
    }
     //MARK: Configuration Methods
    
    func loadData(name:String, repo:String){
        let vc = PullRequestsClient()
        vc.delegate = self
        vc.get(creator: name, repo: repo)
        AppHelper.shared.hudManager.showHud()
    }
}
     //MARK: Client Delegate Methods

extension PullRequestViewController: PullRequestsClientDelegate {
    func success(response: [PullRequest]) {
        array?.addObjects(from: response)
        if (array?.count)! > 0 {
           tableView.reloadData()
        } else {
            AppHelper.shared.alertManager.showAlert()
        }
        
        AppHelper.shared.hudManager.hideHud()
    }
    
    func error(error: NSString) {
        AppHelper.shared.alertManager.showAlertError(error: error)
        AppHelper.shared.hudManager.hideHud()
    }
}

     //MARK: TableView Methods

extension PullRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as! PullRequestTableViewCell
        if let array = array {
            cell.populate(object: array[indexPath.row] as! PullRequest)
        }
        
        return cell
    }
    
    func tableView(_ taxbleView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
}
