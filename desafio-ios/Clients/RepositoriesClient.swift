//
//  RepositoriesClient.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/4/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import Alamofire

protocol RepositoriesClientDelegate{
    func success(response:[Repository])
    func error(error:NSString)
}

class RepositoriesClient: NSObject {
    var delegate:RepositoriesClientDelegate?
    
    func get(page:Int){
        Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)").responseJSON { response in
            do {
                if let JSON = try JSONSerialization.jsonObject(with: response.data!, options: []) as? NSDictionary
                {
                    let responseObject = try Repositories(map: Mapper(json: JSON))
                    self.delegate?.success(response: responseObject.list)
                }
            } catch let error as NSError {
                self.delegate?.error(error: error.localizedDescription as NSString)
            }
        }
    }
    
}

