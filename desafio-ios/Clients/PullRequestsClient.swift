//
//  PullRequestsClient.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 05/01/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import Alamofire

protocol PullRequestsClientDelegate{
    func success(response:[PullRequest])
    func error(error:NSString)
}

class PullRequestsClient: NSObject {
    var delegate:PullRequestsClientDelegate?
    
    func get(creator:String, repo:String){
        Alamofire.request("https://api.github.com/repos/\(creator)/\(repo)/pulls").responseJSON { response in
            do {
                if let JSON = try JSONSerialization.jsonObject(with:response.data!, options:[]) as? NSArray
                    {
                        let dict : NSDictionary = ["list" : JSON]
                        let responseObject = try PullRequests(map: Mapper(json: dict as NSDictionary))
                        self.delegate?.success(response: responseObject.list)
                    }

            } catch let error as NSError {
                self.delegate?.error(error: error.localizedDescription as NSString)
            }
        }
    }
    
}
