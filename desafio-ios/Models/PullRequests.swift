//
//  PullResquests.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 05/01/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

class PullRequests: NSObject {
    private(set) var list: [PullRequest]
    
    required init(map: Mapper) throws {
        list = map.optionalFrom(field: "list") ?? []
    }
}

public class PullRequest: Mappable {
    public private(set) var id: String?
    public private(set) var name: String?
    public private(set) var description: String?
    public private(set) var title: String?
    public private(set) var user: Owner?
    public private(set) var date: String?
    
    required public init(map: Mapper) throws {
        id  = map.optionalFrom(field: "id")
        user = map.optionalFrom(field:"user")
        description  = map.optionalFrom(field: "body")
        title  = map.optionalFrom(field: "title")
        date = map.optionalFrom(field: "created_at")
        
    }
}

