//
//  Repositories.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/4/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

class Repositories: NSObject {
    private(set) var list: [Repository]
    
    required init(map: Mapper) throws {
        list = map.optionalFrom(field: "items") ?? []
    }
}

public class Repository: Mappable {
    public private(set) var id: String?
    public private(set) var name: String?
    public private(set) var description: String?
    public private(set) var forks: Int?
    public private(set) var watchers: Int?
    public private(set) var owner: Owner?
    
    required public init(map: Mapper) throws {
        id  = map.optionalFrom(field: "id")
        name  = map.optionalFrom(field: "name")
        description  = map.optionalFrom(field: "description")
        forks  = map.optionalFrom(field: "forks")
        watchers  = map.optionalFrom(field: "watchers")
        owner = map.optionalFrom(field: "owner")
    }
}
