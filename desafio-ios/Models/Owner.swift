//
//  Owner.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/5/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

public class Owner: Mappable {
    public private(set) var id: String?
    public private(set) var login: String?
    public private(set) var avatar_url: String?

    required public init(map: Mapper) throws {
        id  = map.optionalFrom(field: "id")
        login  = map.optionalFrom(field: "login")
        avatar_url  = map.optionalFrom(field: "avatar_url")
    }
}
