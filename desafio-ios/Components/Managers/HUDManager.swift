//
//  HUDManager.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/6/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import MBProgressHUD

class HUDManager: NSObject {
    let appDelegate = UIApplication.shared.delegate
    
    func showHud(){
        if let window = appDelegate?.window, let navigationController = window?.rootViewController as? UINavigationController {
            guard let viewController = navigationController.viewControllers.first else { return }
            let loadingNotification = MBProgressHUD.showAdded(to: viewController.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
        }
    }
    
    func hideHud(){
        if let window = appDelegate?.window, let navigationController = window?.rootViewController as? UINavigationController {
            guard let viewController = navigationController.viewControllers.first else { return }
            MBProgressHUD.hide(for: viewController.view, animated: true)
        }
        
    }
}
