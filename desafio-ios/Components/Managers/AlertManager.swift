//
//  AlertManager.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/6/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

class AlertManager: NSObject {
    let appDelegate = UIApplication.shared.delegate
    
    func showAlert(){
        if let window = appDelegate?.window, let navigationController = window?.rootViewController as? UINavigationController {
            let alert = UIAlertController(title: "Oops", message: "This repository not have pull Requests", preferredStyle: UIAlertControllerStyle.alert)
            guard let viewController = navigationController.viewControllers.first else { return }
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { UIAlertAction in
                _ = viewController.navigationController?.popViewController(animated: true)
            }))
            
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlertError(error:NSString){
        if let window = appDelegate?.window, let navigationController = window?.rootViewController as? UINavigationController {
            let alert = UIAlertController(title: "Oops", message: error as String, preferredStyle: UIAlertControllerStyle.alert)
            guard let viewController = navigationController.viewControllers.first else { return }
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
            
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}
