//
//  AppHelper.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/6/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
private let instance = AppHelper()

class AppHelper: NSObject {
    let hudManager = HUDManager()
    let alertManager = AlertManager()
    
    class var shared: AppHelper {
        return instance
    }
}
