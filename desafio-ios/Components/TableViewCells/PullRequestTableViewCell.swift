//
//  PullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 05/01/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var titlePullRequest: UILabel!
    @IBOutlet weak var descriptionPullRequest: UILabel!
    @IBOutlet weak var imagePullRequest: UIImageView!
    @IBOutlet weak var nameUserPullRequest: UILabel!
    @IBOutlet weak var dateCreationPullRequest: UILabel!
   
    func populate(object:PullRequest) {
        titlePullRequest.text = object.title
        descriptionPullRequest.text = (object.description?.characters.count)! > 0 ? object.description : "No Description"
        nameUserPullRequest.text = object.user?.login
        imagePullRequest.sd_setImage(with: URL(string:(object.user?.avatar_url)!))
        imagePullRequest.setRounded()
        dateCreationPullRequest.text = formatDateString(string: object.date!)
    }
    
    func formatDateString(string:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = (dateFormatter.date(from:string))
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        if let date = date {
            let formattedDate = (dateFormatter.string(from: date))
            let dateShort = (dateFormatter.date(from:formattedDate))
              return dateFormatter.string(from: dateShort!)
        }
        
        return ""
        
    }
}
