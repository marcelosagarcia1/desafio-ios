//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/5/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {
    @IBOutlet var titleRepository: UILabel!
    @IBOutlet var descriptionRepository: UILabel!
    @IBOutlet var imageRepository: UIImageView!
    @IBOutlet var nameOwner: UILabel!
    @IBOutlet var forksRepository: UILabel!
    @IBOutlet var watchersRepository: UILabel!
    @IBOutlet var watcherImageRepository: UIImageView!
    @IBOutlet var forkImageRepository: UIImageView!
    
    func populate(object:Repository) {
        titleRepository.text = object.name
        descriptionRepository.text = object.description
        nameOwner.text = object.owner?.login
        forksRepository.text = "\(object.forks!)"
        watchersRepository.text = "\(object.watchers!)"
        imageRepository.sd_setImage(with: URL(string:(object.owner?.avatar_url)!))
        imageRepository.setRounded()
        watcherImageRepository.changeImageColor(color: UIColor.orange)
        forkImageRepository.changeImageColor(color: UIColor.orange)
    }
}
