//
//  UIImage+Rounded.swift
//  desafio-ios
//
//  Created by Marcelo Garcia on 1/5/17.
//  Copyright © 2017 Marcelo Garcia. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func changeImageColor(color:UIColor){
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}
